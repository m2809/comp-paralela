# Laboratório 05

## Enunciado do laboratório
Selecione uma das opções abaixo para sugerir estratégias de paralelização para as operações a seguir. Se for possível pensar em mais de uma solução, discuta os prós e contras de cada uma.

1)  Crie um programa em C que multiplica todos os elementos de um array por 4 ou por um valor fornecido pelo usuário;

2) Crie um programa em C que seja capaz de multiplicar dois arrays (a, b) para encontrar o valor: c = a * b.


## Minha solução

**Ítem selecionado**: 1)  Crie um programa em C que multiplica todos os elementos de um array por 4 ou por um valor fornecido pelo usuário.

Executado através do [Repl.it](https://replit.com/).

Problemas para finalizar implementação do paralelismo. Estratégia pensada para paralelizar nos comentários do código main
