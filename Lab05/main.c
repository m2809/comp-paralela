/*
  Pedro Figueiredo Dias
  41990455

  Nesse código:
  1)  Crie um programa em C que multiplica todos os elementos de um array por 4 ou por um valor fornecido pelo usuário;

  Estratégia para paralelizar:
  * Calcular simultaneamente o valor da multiplicação em cada coluna ou cada linha, através de (nesse caso de matriz 3x3) 3 processos diferentes.

  -- não consegui executar paralelamente. Está dando erro
*/

#include <stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<unistd.h>

#define SIZE 3 // Size of the matrix

int main()
{
    int M[SIZE][SIZE]; // Matrix
    int RESULT[SIZE][SIZE]; // Resultant matrix

    int MULTIPLIER;

    int row, col;

    /* Input elements in first matrix*/
    printf("Enter elements in matrix of size 3x3: \n");
    for(row=0; row<SIZE; row++)
    {
        for(col=0; col<SIZE; col++)
        {
            scanf("%d", &M[row][col]);
        }
    }

    /* Input elements in second matrix */
    printf("\nEnter multiplier: \n");
    scanf("%d", &MULTIPLIER);

    for(col=0; col<SIZE; col++)
      RESULT[0][col] = M[0][col] * MULTIPLIER;

    printf("\n\nCalculo da 1ª col = %d %d %d", RESULT[0][0], RESULT[0][1],RESULT[0][2]);

    /*
      Implementação do paralelismo
    */
    int pid = fork();
    int stat;
    if (pid < 0) {
      perror("fork");
    }
    else if (pid == 0) {
      for(col=0; col<SIZE; col++)
        RESULT[1][col] = M[1][col] * MULTIPLIER;

      printf("\n\nProcesso pai calcula 2ª col = %d %d %d", RESULT[1][0], RESULT[1][1],RESULT[1][2]);

      exit(1);
    }
    else {
      for(col=0; col<SIZE; col++)
        RESULT[2][col] = M[2][col] * MULTIPLIER;

      printf("\n\nProcesso filho calcula 3ª col = %d %d %d", RESULT[2][0], RESULT[2][1],RESULT[2][2]);
    }

    if(WIFEXITED(stat))
    {
      /* Print the value of resultant matrix */
      printf("\nMultiplication result = \n");
      for(row=0; row<SIZE; row++)
      {
          for(col=0; col<SIZE; col++)
          {
              printf("%d ", RESULT[row][col]);
          }
          printf("\n");
      } 
    }

    return 0;
}