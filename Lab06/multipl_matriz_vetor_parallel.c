#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

/* 
  Multiplicação matriz-vetor

  Desenvolvido e compilado usando o https://replit.com/
  Pedro Figueiredo Dias - 41990455 (05G)
*/

// y: vetor resultado
// A: matriz
// x: vetor
// m: rows
// n: columns
// thread_count: numero de max threads

int A[2][4] = {{1, 2, 3, 4}, {5, 6, 7, 8}};
int x[2] = {2, 3};
int y[2];

int thread_count;
int m = 2;
int n = 4;

void *Pth_mat_vect(void* rank) {
  long my_rank = (long) rank;
  int i, j;
  int local_m = m / thread_count;
  int my_frist_row = my_rank * local_m;
  int my_last_row = (my_rank + 1) * local_m - 1;

  for (i = my_frist_row; i <= my_last_row; i++) {
    y[i] = 0.0;
    for (j = 0; j < n; j++)
      y[i] += A[i][j] * x[j];
  }

  return NULL;
}

void printY()
{
  for (int i = 0; i < m; i++)
    printf("%d ", y[i]);
}

int main(int agrc, char* argv[]) {
  long thread;
  pthread_t* thread_handles;

  thread_count = strtol(argv[1], NULL, 10);

  thread_handles = malloc(thread_count * sizeof(pthread_t));

  // Criacao da thread: (alocar mem, attr, function, args)
  for (thread = 0; thread < thread_count; thread++)
    pthread_create(&thread_handles[thread], NULL,
      Pth_mat_vect, (void*) thread);

  printf("Hello from the main thread\n");

  for (thread = 0; thread < thread_count; thread++)
    pthread_join(thread_handles[thread], NULL);

  free(thread_handles);
  printY(); /* imprimir resultado */
  return 0;
}