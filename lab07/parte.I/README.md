# Execução lab 07

Compilado com WSL rodando Ubuntu.

Para instalar o OpenMP: `sudo apt-get install libomp-dev`

Para compilar o código: `gcc -g -fopenmp -o omp_hello openmp.c`

## Print da execução

![print da execução lab07](assets/print_execucao.png)


