#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

double f(double x);
void Trap(double a, double b, int n, double* global_result_p);

int main(int argc, char* argv[]){
  double global_result = 0.0;
  double a, b;
  int n;
  clock_t start, end;

  start = clock();

  printf("Enter a, b, and n\n");
  scanf("%lf %lf %d", &a, &b, &n);

  Trap(a, b, n, &global_result);

  end = clock();

  printf("With n = %d trapezoids, our estimat\n", n);
  printf("of the integral from %f to %f = %.14e\n", a, b, global_result);

  double time_spent = (double)(end - start) / CLOCKS_PER_SEC;
  printf("\nelapsed time = %fms\n", time_spent*100);

  return 0;
}

void Trap(double a, double b, int n, double* global_result_p){
  double h;
  double local_a, local_b;

  h = (b - a)/n;
  local_a = a;
  for(local_b = local_a + h; local_b <= b; local_b+=h){
    *global_result_p += (f(local_a) + f(local_b))/2.0;
    local_a += h;
  }
}

double f(double x){
  return exp(x);
}