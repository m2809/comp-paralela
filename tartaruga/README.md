# Problema da Tartaruga

Série de Taylor do logaritmo natural

## Grupo

Pedro Figueiredo - 41990455<br>
Claudio Ramos - 41916565<br>
Ye Wei Jiang - 41926293<br>
Kevin Yao Ji - 41916492

## Entrega

Para testar e avaliar, utilizar a versão final, na raíz do projeto:

* compilar usando: `gcc -g -o serial serial_tartaruga.c -lm`
* executar usando: `./openmp 10 1000000000` (tempo médio na minha máquina 20 segundos)

### Exemplo da exeução:
![Exemplo de execução da final_version](./assets/sample_exec.png)

### Maior valor atingidoa (até o momento):
![Print do maior valor obtido para a aproximação de S](./assets/maior_T_2.png);

Tempo em minutos: 154 minutos (aproximadamente) (2h34 min)

Valor T atingido: 10000000000000 (10^12)

## Outras versões criadas durante o desenvolvimento

Compilar usando gcc (eu usei o WSL): 
* Versão serial: `gcc -g -o serial serial_tartaruga.c -lm`
* Versão PThreads (diretório `/pthreads`): `gcc -g -Wall -o pthread pthread_tartaruga.c -lpthread`
* Versão OpenMP (diretório `/openmp`): `gcc -g -fopenmp -o openmp openmp_tartaruga.c`

Para todas as versões, exceto a serial, executar com: `./openmp n_threads exptectd_t`, onde **expected_t** é o número limite do denominador utilizado no cálculo.

Exemplo: `./openmp 10 1000`

## Referências

- [Measuring time taken by a function with clock_gettime - Stack Overflow](https://stackoverflow.com/questions/3946842/measuring-time-taken-by-a-function-clock-gettime)

