/* Solução com openmp */

/* 
  * Adaptar para deixar rodando, imprimir o tempo, S e T a cada 
    X minutos
*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#ifdef _OPENMP
#include<omp.h>
#endif

#define BILLION 1E9

double Tartaruga(long long expected_t, long long* global_t);

int main(int agrc, char* argv[]) {
  struct timespec start, end;
  clock_gettime(CLOCK_REALTIME, &start);

  double global_result = 0.0;
  long long global_t = 0;
  int thread_count;
  long long expected_t; /* maior valor de T possivel */

  thread_count = strtol(argv[1], NULL, 10);
  expected_t = strtol(argv[2], NULL, 10);

# pragma omp parallel num_threads(thread_count) \
    reduction(+: global_result)
  global_result += Tartaruga(expected_t, &global_t);

  clock_gettime(CLOCK_REALTIME, &end);
  double elapsed_seconds = ( end.tv_sec - start.tv_sec )  + ( end.tv_nsec - start.tv_nsec )  / BILLION;

  printf("global_t %lld\n", global_t);
  printf("for T = %.0lld, and using %d threads\nS = %f\n", expected_t, thread_count, global_result);
  printf("\nelapsed seconds: %lf\n", elapsed_seconds);

  return 0;
} /* main */

double Tartaruga(long long expected_t, long long* global_t)
{
  double my_result = 0;
  double t;

  int my_rank = omp_get_thread_num();
  int thread_count = omp_get_num_threads();

  long long my_n = expected_t/thread_count;
  long long my_first_t = my_rank * my_n;
  long long my_last_t = my_first_t + my_n;

  for (t = my_first_t; t < my_last_t; t++)
  {
#   pragma omp critical
    *global_t += 1;
    my_result += 1/(t+1);
  }
  
  return my_result;
}
