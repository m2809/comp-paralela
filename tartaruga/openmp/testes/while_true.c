#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#ifdef _OPENMP
#include<omp.h>
#endif

#define BILLION 1E9

void Tartaruga(double* t, double* global_result_p);

int main(int agrc, char* argv[]) {
  struct timespec start, end;
  clock_gettime(CLOCK_REALTIME, &start);

  double global_result = 0.0;
  double global_t = 0.0;
  int thread_count;

  thread_count = strtol(argv[1], NULL, 10);

# pragma omp parallel num_threads(thread_count)
  Tartaruga(&global_t, &global_result);

  clock_gettime(CLOCK_REALTIME, &end);
  double elapsed_seconds = ( end.tv_sec - start.tv_sec )  + ( end.tv_nsec - start.tv_nsec )  / BILLION;

  printf("for T = %.0f, and using %d threads\nS = %f\n", global_t, thread_count, global_result);
  printf("\nelapsed seconds: %lf\n", elapsed_seconds);

  return 0;
} /* main */
