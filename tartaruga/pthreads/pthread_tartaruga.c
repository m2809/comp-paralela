/* Resoluçào utilizando pthreads */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include<time.h>

int thread_count = 1;
int expected_t = 1;
double global_t, global_result = 0.0;

#define BILLION 1E9

void* Tartaruga(void* rank);

int main(int argc, char* argv[]) {
  struct timespec start, end;
  clock_gettime(CLOCK_REALTIME, &start);

  long thread;
  pthread_t* thread_handles;
  thread_handles = malloc(thread_count * sizeof(pthread_t));

  thread_count = strtol(argv[2], NULL, 10);
  expected_t = strtol(argv[1], NULL, 10);

  // printf("expected_t = %d\n", expected_t);
  // printf("thread_count = %d\n", thread_count);

  for (thread = 0; thread < thread_count; thread++)
    pthread_create(&thread_handles[thread], NULL, 
    Tartaruga, (void*) thread);

  for (thread = 0; thread < thread_count; thread++)
    pthread_join(thread_handles[thread], NULL);

  clock_gettime(CLOCK_REALTIME, &end);
  double elapsed_seconds = ( end.tv_sec - start.tv_sec )  + ( end.tv_nsec - start.tv_nsec )  / BILLION;

  printf("for T = %.0f, and using %d threads\nS = %f\n", global_t, thread_count, global_result);
  printf("\nelapsed seconds: %lf\n", elapsed_seconds);
  
  free(thread_handles);
  return 0;
} /* main */

void* Tartaruga(void* rank) {
  long my_rank = (long) rank;
  double t; /* tempo */
  double my_t = expected_t/thread_count;
  double my_first_t = my_t * my_rank;
  double my_last_t = my_first_t + my_t;

  // printf("\nExecuting thread %ld\n", my_rank);
  
  for (t = my_first_t; t < my_last_t; t++) {
    // printf("1/%f = %f\n", t, 1/(t+1));
    // printf("global result at this moment = %f\n", global_result);
    global_result += 1/(t+1);
    global_t++;
  }

  return NULL;
} /* Tartaruga */