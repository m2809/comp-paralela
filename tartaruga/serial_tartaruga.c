/* Resolução sem paralelismo */

#include<stdio.h>
#include <stdlib.h>
#include <math.h>
#include<time.h>

#define BILLION 1E9

int main(int argc, char* argv[]) {
  struct timespec start, end;
  clock_gettime(CLOCK_REALTIME, &start);

  double global_result = 0.0; /* valor final calculado */
  double t = 0; /* tempo */ 
  int expected_t;

  expected_t = strtol(argv[1], NULL, 10);

  while (t != expected_t) {
    global_result += 1/(t+1);
    t++;
  }

  clock_gettime(CLOCK_REALTIME, &end);
  double elapsed_seconds = ( end.tv_sec - start.tv_sec )  + ( end.tv_nsec - start.tv_nsec )  / BILLION;

  printf("for T = %.0f \nS = %f\n", t, global_result);
  printf("\nelapsed seconds: %lf\n", elapsed_seconds);

  return 0;
}